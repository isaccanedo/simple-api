package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@ComponentScan(basePackages = "com.example")
@RestController
@SpringBootApplication
public class IsacApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsacApplication.class, args);		
		
	}
	
	@RequestMapping("/hello")
	public String hello() {
		
		return "Bem-vindo Isac ao endpoint hello";
	}

}

package com.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@RequestMapping("/users")
	public String users() {
		return "{\"users\":[{\"name\":\"Isac\", \"country\":\"Brazil\"}," +
		           "{\"name\":\"Canedo\",\"country\":\"Brazil\"}]}";
	}

}
